var app11 = new Vue({
  el: '#app-11',
  data: {
    html: '<p>App11 HTML読み込みサンプル</p>'
  }
})


var app10 = new Vue({
  el: '#app-10',
  data: {
    input: 'サンプル１',
    text: '',
  },
  methods: {
    output: function (){
      this.text = this.input;
    }
  },
})


var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello ほげほげワールド!'
    },
  })

  var app2 = new Vue({
    el: '#app-2',
    data: {
      message: 'You loaded this page on ' + new Date().toLocaleString()
    }
  })

  var app3 = new Vue({
    el: '#app-3',
    data: {
      seen: true
    }
  })

  var app4 = new Vue({
    el: '#app-4',
    data: {
      todos: [
        { text: 'Learn JavaScript' },
        { text: 'Learn Vue' },
        { text: 'Build something awesome' },
        { text: 'Build something awesome' },
        { text: 'Build something awesome' },
      ]
    }
  })

  var app5 = new Vue({
    el: '#app-5',
    data: {
      message: 'Hello Vue.js!'
    },
    methods: {
      reverseMessage: function () {
        this.message = this.message.split('').reverse().join('');
        this.message = "ほげほげほ〜";
      }
    }
  })

// todo-item と呼ばれる新しいコンポーネントを定義
Vue.component('todo-item0', {
  // todo-item コンポーネントはカスタム属性のような "プロパティ" で受け取ります。
  // このプロパティは todo と呼ばれます。
  template: '<li>ほげほげほげら</li>'
})


var app6 = new Vue({
    el: '#app-6',
})

// todo-item と呼ばれる新しいコンポーネントを定義
Vue.component('todo-item', {
  // todo-item コンポーネントはカスタム属性のような "プロパティ" で受け取ります。
  // このプロパティは todo と呼ばれます。
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
})

var app7 = new Vue({
  el: '#app-7',
  data: {
    groceryList: [
      { id: 0, text: 'ほげほげ' },
      { id: 1, text: 'はげはげ' },
      { id: 2, text: 'ふげふげ' }
    ]
  }  
})

var app8 = new Vue({
  el: '#app-8',
  data: {
    num: '0'
  }
});

var app9 = new Vue({
  el: '#app-9',
  data: {
    text: 'サンプル'
  }
})